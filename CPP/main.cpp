#include <cstdint>
#include <functional>
#include <iostream>
#include <random>
#include <cstdlib>
#include <ctime>
#include <vector>

#include "check_order.hpp"

/** sorting algrithm */
#include "bubble.hpp"
#include "selection.hpp"

/** display all elements of list before/after. */
//#define _DEBUG 0


void init_vec(std::vector<int64_t> &vec, int64_t N) {
    /** random value generator */
    std::random_device rnd;
    /** 32bit Mersenne twister */
    std::mt19937 mt(rnd());
    // randomize vector
    for (int64_t i=0; i<N; ++i) {
        vec.push_back(mt());
    }
}


void measure_sorting_time(
        int64_t N,
        std::function<void(std::vector<int64_t>&, const bool)> sort,
        bool order)
{
    // randomize vector
    std::vector<int64_t> vec;
    init_vec(vec, N);

    // display vector before sorting
    std::cout << "Before sorting:" << std::endl;

    if (check_order(vec, order)) {
        std::cerr << "Error: vector is not randamized." << std::endl;
        std::exit(EXIT_FAILURE);
    }

#ifdef _DEBUG
    for (int64_t elem: vec) {
        std::cout << elem << std::endl;
    }
#endif

    // measure sorting time
    std::cout << "sorting...." << std::endl;
    clock_t tstart = clock();
    sort(vec, order);
    clock_t dt = clock() - tstart;
    std::cout << "Time: " << dt << " [ms]" << std::endl;

    std::cout << "After sorting: " << std::endl;

    if (check_order(vec, order)) {
        std::cout << "Sorted?: OK" << std::endl;
    } else {
        std::cout << "Sorted?: NG" << std::endl;
    }

    // display vector after sorting
#ifdef _DEBUG
    for (int64_t elem: vec) {
        std::cout << elem << std::endl;
    }
#endif
}


int main(void)
{
    /** number of elemens of list */
    const int64_t N = 10000;
    /** sorting order (true: ascending) */
    const bool order = true;

    // asign rand values
    std::vector<int64_t> vec;

    std::cout << "Bubble sort -----------------" << std::endl;
    measure_sorting_time(N, bubble_sort, order);

    std::cout << "Selection sort --------------" << std::endl;
    measure_sorting_time(N, selection_sort, order);

    return 0;
}

