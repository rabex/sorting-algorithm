#ifndef SELECTION_H_INCLUDED
#define SELECTION_H_INCLUDED

#include <cstdint>
#include <vector>

void selection_sort(std::vector<int64_t> &vec, const bool ascending);

#endif

