# Makefile for cl.exe & link.exe on Windows
CXX=cl.exe
CXXFLAGS=/EHsc /c
LINK=link.exe
RM=del

main.exe: main.obj bubble.obj selection.obj check_order.obj
	$(LINK) main.obj bubble.obj selection.obj check_order.obj

main.obj: main.cpp
	$(CXX) $(CXXFLAGS) main.cpp

bubble.obj: bubble.cpp
	$(CXX) $(CXXFLAGS) bubble.cpp

selection.obj: selection.cpp
	$(CXX) $(CXXFLAGS) selection.cpp

check_order.obj: check_order.cpp
	$(CXX) $(CXXFLAGS) check_order.cpp

.PHONY: clean
clean:
	-$(RM) *.obj *.exe

.PHONY: all
all: clean main.exe
