#include "bubble.hpp"
#include <cstdint>
#include <cstdio>
#include <iostream>
#include <vector>

void bubble_sort(std::vector<int64_t> &vec, const bool ascending)
{
    int64_t n = vec.size();
    for (int64_t i=0; i<n; ++i) {
        for (int64_t j=0; j<n-1-i; ++j) {
            if (ascending) {
                if (vec[j] > vec[j+1]) {
                    std::swap(vec[j], vec[j+1]);
                }
            } else {
                if (vec[j] < vec[j+1]) {
                    std::swap(vec[j], vec[j+1]);
                }
            }
        }
    }
}
