#include "check_order.hpp"

#include <cstdint>
#include <iostream>
#include <vector>

//#define _DEBUG 0

bool check_order(const std::vector<int64_t> &vec, const bool ascending)
{
    std::vector<int64_t>::const_iterator itr;
    for (itr = ++(vec.cbegin()); itr != vec.cend(); ++itr) {
        if (ascending == true) {
            if (*std::prev(itr) > *itr) {
#ifdef _DEBUG
                // NOT ascending order
                std::cout << *std::prev(itr) << " -> " << *itr << " is not ascending order." << std::endl;
#endif
                return false;
            }
        } else {
            if (*std::prev(itr) < *itr) {
#ifdef _DEBUG
                // NOT descending order
                std::cout << *std::prev(itr) << " -> " << *itr << " is not descending order." << std::endl;
#endif
                return false;
            }
        }
    }
    return true;
}
