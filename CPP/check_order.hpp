#ifndef CHECK_ORDER_HPP_INCLUDED
#define CHECK_ORDER_HPP_INCLUDED

#include <cstdint>
#include <vector>

/**
 * <summary>check if given list is in ascending order.</summary>
 * <argments>
 *      vec: given vector rand values are stored.
 *      ascending:
 *          true:   check if ascending
 *          false:  check if descending
 * </argments>
*/
bool check_order(const std::vector<int64_t> &vec, const bool ascending);

#endif
