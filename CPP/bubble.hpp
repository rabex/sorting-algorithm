#ifndef BUBBLE_H_INCLUDED
#define BUBBLE_H_INCLUDED

#include <cstdint>
#include <vector>

void bubble_sort(std::vector<int64_t> &vec, const bool ascending);

#endif
