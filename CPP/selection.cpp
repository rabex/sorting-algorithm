#include "selection.hpp"

#include <cstdint>
#include <iostream>
#include <vector>

//#define _DEBUG

void selection_sort(std::vector<int64_t> &vec, const bool ascending)
{
#ifdef _DEBUG
    std::cout << "function: selection_sort()" << std::endl;
#endif

    int64_t n = vec.size();

#ifdef _DEBUG
    std::cout << "vec:" << std::endl;
    for (int64_t j=0; j<n; ++j) {
        printf("[%03I64d] %I64d \n", j, vec[j]);
    }
    std::cout << std::endl;
#endif

    for (int64_t i=0; i<n-1; ++i) {
        if (ascending) {
            int64_t min = i;
            for (int64_t j=i+1; j<n; ++j) {
                if (vec[j] < vec[min]) {
                    min = j;
                }
            }
#ifdef _DEBUG
            std::cout << "i = " << i << std::endl;
            std::cout << "min = " << min << std::endl;
            std::cout << "vec[min] = " << vec[min] << std::endl;
#endif
            std::swap(vec[min], vec[i]);
        } else {
            int64_t max = i;
            for (int64_t j=i+1; j<n; ++j) {
                if (vec[j] > vec[max]) {
                    max = j;
                }
            }
#ifdef _DEBUG
            std::cout << "i = " << i << std::endl;
            std::cout << "max = " << max << std::endl;
            std::cout << "vec[max] = " << vec[max] << std::endl;
#endif
            std::swap(vec[max], vec[i]);
        }

#ifdef _DEBUG
        for (int64_t j=0; j<n; ++j) {
            printf("[%03I64d] %I64d \n", j, vec[j]);
        }
        std::cout << std::endl;
#endif
    }
}

