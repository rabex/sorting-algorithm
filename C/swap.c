#include "swap.h"

#include <stdint.h>

void swap(int64_t *a, int64_t *b)
{
    int64_t tmp = *a;
    *a = *b;
    *b = tmp;
}

