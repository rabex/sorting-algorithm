#include "bubble.h"

#include <stdint.h>
#include "swap.h"

void bubble_sort(int64_t *list, int64_t n, int64_t order)
{
    for (int64_t i=0; i<n; ++i) {
        for (int64_t j=0; j<n-1-i; ++j) {
            if (order > 0) {
                // ascending
                if (list[j] > list[j+1]) {
                    swap(&list[j], &list[j+1]);
                }

            } else {
                // descending
                if (list[j] < list[j+1]) {
                    swap(&list[j], &list[j+1]);
                }
            }
        }
    }
}

