#ifndef CHECK_ORDER_H_INCLUDED
#define CHECK_ORDER_H_INCLUDED

#include <stdint.h>

/**
 * <summary>check if given list is in ascending order.</summary>
 * <argments>
 *      list: given list
 *      n:  length of list
 *      acending: 0 -> decending, not 0 -> acending order.
 * </argments>
*/
int64_t check_order(int64_t *list, int64_t n, int64_t ascending);

#endif
