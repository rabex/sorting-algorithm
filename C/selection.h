#ifndef SELECTION_H_INCLUDED
#define SELECTION_H_INCLUDED

#include <stdint.h>

void selection_sort(int64_t *list, int64_t n, int64_t order);

#endif
