#ifndef SWAP_H_INCLUDED
#define SWAP_H_INCLUDED

#include <stdint.h>

void swap(int64_t *a, int64_t *b);

#endif
