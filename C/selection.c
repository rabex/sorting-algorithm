#include "selection.h"

#include <stdint.h>
#include "swap.h"

void selection_sort(int64_t *list, int64_t n, int64_t order)
{
    for (int64_t i=0; i<n-1; ++i) {
        int64_t pos = i;
        for (int64_t j=i+1; j<n; ++j) {
            if (order > 0) {
                // ascending
                if (list[j] < list[pos]) {
                    pos = j;
                }

            } else {
                // descending
                if (list[j] > list[pos]) {
                    pos = j;
                }
            }
        }
        swap(&list[pos], &list[i]);
    }
}

