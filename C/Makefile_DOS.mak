# Makefile for cl.exe & link.exe on Windows
CC=cl.exe
CCFLAGS=/c
LINK=link
RM=del

main.exe: main.obj bubble.obj selection.obj swap.obj check_order.obj
    $(LINK) main.obj bubble.obj selection.obj swap.obj check_order.obj


main.obj: main.c
    $(CC) $(CCFLAGS) main.c

bubble.obj: bubble.c
    $(CC) $(CCFLAGS) bubble.c

selection.obj: selection.c
	$(CC) $(CCFLAGS) selection.c

swap.obj: swap.c
	$(CC) $(CCFLAGS) swap.c

check_order.obj: check_order.c
	$(CC) $(CCFLAGS) check_order.c

.PHONY: clean
clean:
    $(RM) *.obj *.exe
