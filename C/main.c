#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include "check_order.h"

/** sorting algrithm */
#include "bubble.h"
#include "selection.h"

/** display all elements of list before/after. */
#define _DEBUG 0

void init_list(int64_t *list, int64_t n)
{
    srand((unsigned) time(NULL));

    for (int64_t i=0; i<n; ++i) {
        list[i] = rand();
    }
}

void measure_sorting_time(const n, void (*sort)(int64_t*, int64_t, int64_t), const int64_t order)
{
    int64_t *list = (int64_t *) malloc(sizeof(int64_t) * n);
    if (list == NULL) {
        fprintf(stderr, "Error: Mamory allocation error.\n");
        exit(EXIT_FAILURE);
    }
    init_list(list, n);

    if (check_order(list, n, order)) {
        fprintf(stderr, "Error: list is not randomized.\n");
        exit(EXIT_FAILURE);
    }

#ifdef _DEBUG
    for (int i=0; i<n; ++i) {
        printf("%I64d\n", list[i]);
    }
#endif

    clock_t tstart = clock();
    sort(list, n, order);
    clock_t dt = clock() - tstart;
    printf("Time: %f [s]\n", (double) dt / CLOCKS_PER_SEC);

    if (check_order(list, n, order)) {
        printf("Sorted?: OK\n");
    } else {
        printf("Sorted?: NG\n");
    }

#ifdef _DEBUG
    for (int i=0; i<n; ++i) {
        printf("%I64d\n", list[i]);
    }
#endif

    free(list);
    list = NULL;
}

int main(void)
{
    int64_t *list;

    /** number of elemens of list */
    const size_t N = 10;

    /** check order in acending order (a>0:ascending, a<=0:decending) */
    const int64_t ASC_ORDER = 0;

    printf("Bubble sort ---------------------\n");
    measure_sorting_time(N, bubble_sort, ASC_ORDER);

    printf("Selection sort ------------------\n");
    measure_sorting_time(N, selection_sort, ASC_ORDER);

    return 0;
}

