#include "check_order.h"

#include <stdint.h>

int64_t check_order(int64_t *list, int64_t n, int64_t ascending)
{
    for (int64_t i=0; i<n-1; ++i) {
        if (ascending != 0) {
            // acending order
            if (list[i] > list[i+1]) {
                return 0; // false
            }
        } else {
            // descending order
            if (list[i] < list[i+1]) {
                return 0; // false
            }
        }
    }
    return 1; // true
}
