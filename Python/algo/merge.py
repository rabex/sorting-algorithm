def _merge(listA, listB):
    if not listB:
        return
    for elemB in listB:
        pos = 0
        while pos < len(listB) and listA[pos] < elemB:
            pos += 1
        listA.insert(pos, elemB)


def merge_sort(lst):
    ''' Merge sort
    '''
    if len(lst) < 2:
        return lst
    else:
        center = int(len(lst) / 2)
        listA = merge_sort(lst[:center])
        listB = merge_sort(lst[center:])
        _merge(listA, listB)
        del lst[:]
        lst.append(listA)
