def selection_sort(array):
    ''' Selection sort
    '''
    for i in range(len(array)-1):
        mini = i
        for j in range(i, len(array)):
            if array[j] < array[mini]:
                mini = j
        # swap
        array[i], array[mini] = array[mini], array[i]


