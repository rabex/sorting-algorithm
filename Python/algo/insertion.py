def insertion_sort(array):
    ''' Insertion sort
    '''
    if len(array) < 1:
        return
    # target: index to be moved
    for target in range(1, len(array)):
        # search position which array[target] to be located on
        pos = target
        while pos > 0:
            if array[pos-1] > array[target]:
                pos -= 1
            else:
                break
        # insert
        array.insert(pos, array.pop(target))
        # swap: xxxxx
        #array[target], array[pos] = array[pos], array[target]

