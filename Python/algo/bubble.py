def bubble_sort(array):
    ''' Bubble sort
    '''
    for j in range(0, len(array)):
        ascending = True
        for i in range(1, len(array)):
            if array[i-1] > array[i]:
                # swap
                array[i-1], array[i] = array[i], array[i-1]
                ascending = False
        if ascending == True:
            break
