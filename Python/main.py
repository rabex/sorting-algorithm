import numpy
import copy
import time
import argparse

from algo import bubble
from algo import insertion
from algo import selection
from algo import merge

def generage_list(n):
    """ Generate array of ramdom values
    """
    return list(numpy.random.randint(0, 65535, n))

def check_ascending_order(lst):
    """ Check if given array is lining in ascending order.
    """
    result = True
    for i in range(1, len(lst)):
        if lst[i-1] > lst[i]:
            result = False
    return result

def measure_sorting(lst, sort):
    """ Measure time for sorting list.
    """
    lst_copied = copy.deepcopy(lst)
    print("Before: Acending order?: ", check_ascending_order(lst_copied))
    print(lst_copied)
    start = time.time()
    sort(lst_copied)
    elapsed_time = time.time() - start
    print("After:  Acending order?: ", check_ascending_order(lst_copied))
    print(lst_copied)
    print("Time: {0} ms.".format(elapsed_time))


if __name__ == "__main__":
    """ Main module
        input argments and sort with selected algorithm.
    """
    # set shape of argments
    parser = argparse.ArgumentParser()
    parser.add_argument(
            "n",
            help = "(int) length of list",
            type = int)
    parser.add_argument(
            "algo",
            help = "(str) sorting algorithm (bubble, insertion, selection, merge)",
            type = str)

    # get argments
    args = parser.parse_args()

    # check if argment n not too small
    if args.n < 2:
        print("Error: n must to be > 1.")
        exit()

    # generate list and fill on ramdom values
    lst = generage_list(args.n)

    # select and execute sorting algorithm
    if args.algo == "bubble":
        measure_sorting(lst, bubble.bubble_sort)
    elif args.algo == "insertion":
        measure_sorting(lst, insertion.insertion_sort)
    elif args.algo == "selection":
        measure_sorting(lst, selection.selection_sort)
    elif args.algo == "merge":
        measure_sorting(lst, merge.merge_sort)
    else:
        print("Error: Unknown argment: ", arg)
        exit()

